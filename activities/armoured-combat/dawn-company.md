---
sidebar: sidebar-armouredcombat
title: The Dawn Company
prevurl: /Activities/ArmouredCombat/DawnCompany/
---

<ul class="list-inline text-center">
  <li class="list-inline-item pe-3"><a href="{% link activities/armoured-combat/dawn-company-charter.md %}" class="btn btn-primary">Charter</a></li>
  <li class="list-inline-item pe-3"><a href="https://www.youtube.com/channel/UC0GeuIgYXkIQ9Mg3BQzC4_w/" class="btn btn-primary">Youtube channel</a></li>
  <li class="list-inline-item pe-3"><a href="https://www.facebook.com/groups/230692704699074/" class="btn btn-primary">Facebook group</a></li>
  <li class="list-inline-item pe-3"><a href="https://docs.google.com/forms/d/e/1FAIpQLSe8CqjIHlWtCdSs3e9MroIE5KiuL9uCPqnagzgWH5VbsLeRUA/viewform" class="btn btn-primary">Apply to join</a></li>
</ul>


The Dawn Company is a group formed **to promote, encourage and champion armoured combat in Insulae Draconis.** While regular practices take place in several areas of the principality, we have many more fighters who do not have access to a regular practice within reasonable distance. One of the main purposes of the Dawn Company is to provide advice, help, answers to questions, and even mentorship to those who are eager to develop in our sport but are limited by their circumstances. 

Our second main purpose is to provide a community, primarily for our fighters, but also for those who support our fighters by a number of non-combat activities such as marshalling, listkeeping, waterbearing, research, heralding, making gear or making heraldic display, and so on. As the principality is divided between geographically removed islands, it is not always easy to meet up, particularly in the current circumstances of social distancing. Further, there are many who are even ordinarily not able to travel much for a number of different reasons.

Related to both of the above points, our third purpose is to celebrate our love and joy of armoured combat. We approach it firmly inspired by the chivalric ideals typical of the SCA, and rooted in ideals of inclusivity and diversity. While armoured combat, due to its nature, requires a certain level of physical ability, all of our members are warmly welcome regardless of their gender identity, sexuality, ethnicity, ability or disability, cultural or religious background.

We are fully independent of any of the Kingdom and Principality award orders and any other Kingdom and Principality structures. We operate under the blessing of Their Majesties Drachenwald and Their Highnesses Insulae Draconis.  

We are happy to welcome members all over the Kingdom and even in the Known World, but our focus area of activity lies in Insulae Draconis. 

In addition to provision of training, advice and community, we will be holding special tourneys and an annual Company Feast.  

In order to encourage our combatants to strive further, the Company offers them an opportunity to progress through ranks. All fighting members start off as Initiates, and may progress on to Yeoman, Serjeant, Vingtenaur and the top rank of Centenaur. The progress requirements are available in our Charter.  

As we are still brand new, our documents (including our Charter) and our processes and activities are very much in development. When the social distancing period is over and we are free to have physical events again, we will hold our first Company Feast, at which we will formally launch the Company, hear the roll of new members, and formally elect our administrative personnel. 

For the moment, in order to keep the Company going, we have decided upon acting officers, who, together with the rest of the Company’s founding members, are delighted to help you with any questions or suggestions you may have. 


You may contact the Company at **dc@insulaedraconis.org**.

If you wish to make a formal application to join the Dawn Company as a full member, [you can do so here](https://docs.google.com/forms/d/e/1FAIpQLSe8CqjIHlWtCdSs3e9MroIE5KiuL9uCPqnagzgWH5VbsLeRUA/viewform).


<img src="/images/armoured-combat/dc-derlington.jpg" class="rounded m-2" alt="" />

Company Scribe and Acting Primus  
Lord Alexander of Derlington 


Company Chronicler  
Lady Alessandra di Riario


<img src="/images/armoured-combat/dc-yannick.jpg" class="rounded m-2" alt="" />

Founding member  
Viscount Yannick of Normandy, OP


<img src="/images/armoured-combat/dc-agnes.jpg" class="rounded m-2" alt="" />

Founding member  
Lady Agnes des Illes 

