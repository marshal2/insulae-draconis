---
title: Heraldry
subtitle: What is heraldry, and how do I get involved?
banner: /images/banner/banner-over-raglan.jpg
prevurl: /Activities/Heraldry/
---

<blockquote class="testimonial">
<p>
  Before the Internet, before Google..there were heralds.  Heralds of the middle ages were the multipurpose communication, messaging and record-keeping
  &#39;systems&#39; of choice for the nobility.
  The earliest heralds *may have been* minstrels: literate travellers, accustomed to learning and
  remembering stories and lineages, ideal for carrying messages and passing on news. Other heralds
  may have served in fighting forces for their local knight or baron: someone who recorded names,
  numbers of combatants, and their respective coats of arms in a roll of arms, who could advise their
  commander of who was on the muster.
</p>
<p>
  In the late middle ages, heralds took on new roles for advising the crown, acting as ambassadors,
  allowed diplomatic immunity to allow them to convey messages to allies and enemies.
</p>
<p>
  In the Current Middle Ages heralds continue to support the populace and our royals by announcing
  news, recording and registering names and arms, and keeping records. Happily, we now share that work with computers and the internet: it&#39;s no longer an exclusive
  preserve of a handful of noblemen, but available to everyone. You too can release your inner herald!
</p>
<cite>
Dame Genevieve la flechiere, Mynydd Gwyn&nbsp;<br />
<a href="{% link activities/heraldry/before-the-internet.md %}">Read the full article</a>
</cite>

</blockquote>

Heralds bring magic to the society: we often turn things from a modern experience into an authentic medieval one. We do this in several ways.

[Heraldry in the SCA]({% link activities/heraldry/heraldry-in-the-sca.md %})

<img src="/images/heraldry/heraldryandhelms.jpg" class="rounded shadow float-md-end m-2" alt="" />

## Book Heraldry

This is the study and research of names (onomastics) and devices (coats of arms), helping people register their personae (the medieval person that they are acting as within the Society) names and devices with the College of Heralds. 

[About Registering Names or devices]({% link activities/heraldry/register.md %}) -
[How to submit a name or device (Drachenwald Website)](https://drachenwald.sca.org/offices/herald/submittingnamesheraldry/)

## Field Heraldry

This is calling announcements at events - so we can all forget our phones for a while - and heralding tourneys, announcing the progress and the victors to the assembled crowd. We also use heraldry to decorate our halls, fields and equipment, from tabards to chests to thrones.

[War tabard/surcoat design]({% link activities/heraldry/id-tabard.html %}) -
[War standard design]({% link activities/heraldry/id-banner.html %}) -
[Cross stitch patterns]({% link activities/heraldry/cross-stitch.html %})

## Court Heraldry
This is running formal courts effectively and enjoyably, getting business done and adding to the pageantry of the scene. Here you become the voice of the Crown, making the day very special for those you call upon.

[Order of Precedence](http://op.drachenwald.sca.org/awards) Scroll to the Insulae Draconis awards.

[Insulae Draconis Book of Ceremonies](https://insulaedraconis.gitlab.io/ceremonies/) (and the [source repository](https://gitlab.com/insulaedraconis/ceremonies))

[Awards in Insulae Draconis]({% link activities/heraldry/awards.md %}) and [the Kingdom of Drachenwald](http://www.drachenwald.sca.org/offices/herald/drachenwaldawardsorders/)

[Behaviour around the royal couple]({% link coronet/behaviour-around-royal-couple.md %})

[Founding Order members]({% link activities/heraldry/founding-members.html %})

## Banner and badge images

### JPEG Format

[Populace Badge]({% link activities/heraldry/files/populace-badge-a.jpg %})  
[Populace Badge]({% link activities/heraldry/files/populace-badge-b.jpg %})  
[Populace Badge]({% link activities/heraldry/files/populace-badge-c.jpg %})  

### SVG Format

[Nautical Standard]({% link activities/heraldry/files/Insulae_Draconis_-_Nautical_Standard.svg %})  
[Populace Badge - Joscelyn]({% link activities/heraldry/files/Insulae_Draconis_Populace_Badge_Joscelyn.svg %})  
[Populace Badge - BeowulfOT]({% link activities/heraldry/files/Insulae_Draconis_Populace_Badge_BeowulfOT.svg %})  
[Populace Badge - Old London]({% link activities/heraldry/files/Insulae_Draconis_Populace_Badge_Old_London.svg %}) 


<hr>

[SCA Heraldry Pages](http://heraldry.sca.org/)

For heralds taking registrations: [heraldic receipt]({% link activities/heraldry/files/heraldic_receipt_id.pdf %}) (PDF)