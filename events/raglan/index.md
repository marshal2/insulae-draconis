---
title: "Virtual Raglan Fair"
subtitle: "6-9 August 2020"
banner: /images/banner/amphelise_raglan.jpg
---
<h3 style="text-align:center;">
THANK YOU for attending Virtual Raglan!<br /><br />
<a href="{% link events/raglan/2020.md %}" class="btn btn-primary">Read the post-event report</a>
</h3>


Event stewards:  
Sela de la Rosa  
Amphelise de Wodeham  
Yannick of Normandy
