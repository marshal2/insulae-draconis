---
title: "The Coronet: Prince &amp; Princess of Insulae Draconis"
subtitle: Meet our Prince and Princess
prevurl:
  - /prince/progress.html
  - /Coronet/index.html
---

<table class="table table-striped">
    <tbody>
      <tr>
        <td width="50%" style="text-align: right">
          <h2>Prince</h2>
          <strong>Avery of Westfall</strong><br>
          <a href="mailto:prince@insulaedraconis.org" >prince@insulaedraconis.org</a>
        </td>
        <td width="50%" style="text-align: left"><h2>Princess</h2>
            <strong>Zoë of Enstone</strong><br>
          <a href="mailto:princess@insulaedraconis.org">princess@insulaedraconis.org</a> <br></td>
      </tr>
      <tr>
        <td colspan="2" style="text-align: center">
          <img width="400" height="377" src="/coronet/images/avery-zoe.jpg" class="rounded shadow m-3" alt="Prince and Princess of Insulae Draconis"><br>
        </td>
      </tr>
      <tr>
        <td>His Highness is interested in leatherwork, listening to bards, sampling brewing, revels, period environments, archery, rapier, and armoured combat.</td>
        <td>Her Highness is interested in dancing, learning about different periods of garb, historical hairstyling and makeup, embroidery, archery, brewing, cooking, and baking.</td>
      </tr>
      <tr>
        <td>His Highness likes newcomers, laughter, beautiful courts, when the crowd can tell who won a fight.</td>
        <td>Her Highness likes dancing, trying new skills, listening to stories, and activities that bring a wide range of people together to make them feel included.</td>
      </tr>
      <tr>
        <td>
        His Highness would like events to feature accurate and easy-to-understand information on the Calendar and Facebook for newcomers, comfortable places to socialise.
        </td>
        <td>
        Her Highness would like events to feature dancing & dance lessons! Printed timetables for the activities.
        </td>
      </tr>
      <tr>
        <td>
         His Highness dislikes anything that makes it hard for newcomers to get involved, being the reason people are late.
        </td>
        <td>
         Her Highness dislikes not knowing what time she needs to be somewhere, rushing to get ready.
        </td>
      </tr>
      <tr>
        <th colspan="2" style="text-align: center"><i>Regarding food</i></th>
      </tr>
      <tr>
        <td>
         Please feed his Highness: Red meat, salami, cheese, vegetables, granola, streaky bacon, pastries, beer, mead, whiskey, strong coffee with cream, and stronger licorice.
        </td>
        <td>
         Please feed her Highness: Vegetables, berries, chickpeas, oats, chicken, stews, salads, dark chocolate, Scotch whisky, sweets, and decaffeinated tea.
        </td>
      </tr>
      <tr>
        <td>
         Please don't feed his Highness: Shellfish, sugary drinks.
        </td>
        <td>
         Please don't feed her Highness: Caffeine, strong fish (e.g. mackerel, salmon), oranges, nor orange juice.
        </td>
      </tr>

  </tbody>
  </table>
  
  {% comment %}

  <br>
  <h2>The Royal Household</h2>
 <table width="500" cellspacing="0" cellpadding="2">
    <tbody>
      <tr>
        <td><strong>Armoured Champion</strong></td>
        <td></td>
      </tr>
      <tr>
        <td><strong>Court Musician</strong></td>
        <td> </td>
        </tr>
      <tr>
        <td><strong>Cellarer</strong></td>
        <td></td>
        </tr>
      <tr>
        <td><strong>Keeper of the Regalia (Lough Devnaree)</strong></td>
        <td></td>
      </tr>
      <tr>
        <td><strong>Rapier Champion</strong></td>
        <td></td>
      </tr>
      <tr>
        <td><strong>Chief Lady in Waiting</strong></td>
        <td></td>
      </tr>
      </tbody>
  </table>

{% endcomment %}

<br />

<div id="mainContent">

  <h2><a name="progress"></a>Progress of the Prince and Princess</h2>

  <p>
  Events at which their Highnesses will be in attendance are <a href="{% link events/index.html %}">marked with a Crown icon in the Principality Calendar</a>.
  </p>

  <br />

  <h2>What you need to know</h2>

  <p>The Prince and Princess of Insulae Draconis represent the King and Queen of Drachenwald in the Principality. As such ceremonial representatives of the Crown of Drachenwald, the Prince and Princess should be treated as any other royal couple in the Kingdom. In essence, be courteous and respectful of what the Prince and Princess are representing and of the fact that their time is not their own. Much of each event they attend will be spent officiating, being in meetings, or just listening to people's opinions. Read more about how to <a href="{% link coronet/behaviour-around-royal-couple.md %}">behave around the Prince &amp; Princess</a> including at Court.</p>

  <p>The Prince and Princess will hold Court at most events at which they are in attendance. They may have announcements for public business but they also are responsible for bestowing Awards on members where the people of Insulae Draconis has recommended that a member should be so honoured. Insulae Draconis has many awards to reward people for hard work, skill and courtesy. You can <a href="{% link activities/heraldry/awards.md %}">read more about awards</a>, <a href="http://op.drachenwald.sca.org/"> check whether someone already has an award</a> and then <a href="{% link coronet/recommend.html %}">make an award recommendation</a></p>

  <br />

  <h2>Becoming Prince and Princess</h2>

  <p>An armoured combat Tournament is fought every nine months to determine who will be the Prince and Princess.  Any authorised armoured fighter who is a fully paid member of the SCA and who has the support of a consort (also a fully paid up member) can fight.  The tournament will be officially announced and fighter must give formal notification of their intention to enter with a  <a href="{% link coronet/coronet-tourney.md %}#submit-a-letter-of-intent">Letter of Intent</a>.</p>

  <p class="text-center"><a class="btn btn-primary" href="{% link coronet/past.md %}">Our previous Princes and Princesses</a></p>
                        

</div>
