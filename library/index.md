---
prevurl: /Library/
title: Insulae Draconis Library
banner: /images/banner/scribalDeskTretower.jpg
---

# Publications

- [The Baelfyr Monthly Newsletter]({% link baelfyr/index.md %})

# Newcomers Guides

- [Welcome to Newcomers]({% link newcomers/index.md %})
- [Meet the Chatelaine (responsible for helping newcomers)]({% link newcomers/chatelaine.md %})
- [What to expect at your first event]({% link newcomers/what-to-expect.md %})
- [Jargon buster]({% link newcomers/jargonbuster.md %})
- [Why I joined: our members share their stories]({% link newcomers/why-i-joined.md %})
- [What is Court and what is expected of me?]({% link coronet/what-is-court.md %})
- [What do people mean when they say Recommend someone for an award?]({% link activities/heraldry/awards.md %})

# Howtos

- [Personas]({% link library/howtos/adopt-a-persona.md %})
- [Heraldry in the SCA]({% link library/howtos/heraldry-in-sca.md %})
- [Court and Tourney Heraldry]({% link library/howtos/court-and-tourney-heraldry.md %})

# Charters
- [Fibre Guild Charter]({% link library/charters/fibre-guild-charter.md %})

# Group resources

- [How to be a Group Seneschal]({% link library/group-resources/shire-seneschal.md %})
- [How to be a Group Exchequer]({% link library/group-resources/shire-exchequer.md %})
- [How to be a Group Chatelaine]({% link library/group-resources/shire-chatelaine.md %})
- [How to be a Group MoAS]({% link library/group-resources/shire-moas.md %})
- [How to be a Group Captain of Archers]({% link library/group-resources/shire-captain-of-archers.md %})
- [How to be a Group Knight Marshal]({% link activities/armoured-combat/how-to-knight-marshal.md %})
- [Some tips and tricks running an event]({% link library/howtos/running-an-event.md %})
- [Bidding for a Principality event]({% link library/group-resources/bidding-for-events.md %})
- [Some things to think about if you wish to set up a group]({% link library/group-resources/how-to-setup.md %})
- [Drachenwald Code of Conduct](https://drachenwald.sca.org/offices/seneschal/files/DrachenwaldCodeofConductv1.0June2020.pdf)

# History
- [Past Prince and Princesses]({% link coronet/past.md %})

# Principality Law
- [Principality Law - PDF]({% link library/publications/ID_Law.pdf %})

# Forms, signin sheets and waivers

- [Sign In Sheets (for events) Word Doc]({% link library/publications/signinsheet.docx %})

- [Insulae Draconis Event Sign-in Excel Spreadsheet]({% link library/publications/id-sign-in.xlsx %})
